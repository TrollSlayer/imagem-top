# Image TOP
#### Rodrigo de Oliveira Mauricio

### Para rodar

``` 
$ git clone https://gitlab.com/TrollSlayer/imagem-top.git
$ cd imagem-top
$ composer update
$ symfony serve
```

* Link: http://127.0.0.1:8000/

### Estrutura do banco de dados

![](bd.jpg)

