<?php

namespace App\Form;

use App\Entity\Imagem;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class ImageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('img', FileType::class, [
				'label' => 'Imagem',
				'mapped' => false,
				'required' => false,
				'constraints' => [
					new File([
							'maxSize' => '16m',
							'mimeTypes' => [
								'image/jpeg',
							],
							'mimeTypesMessage' => 'Selecione um arquivo válido',
					])
				],
			])
			->add('tamanho', ChoiceType::Class, [
				'choices' => [
					'10x15: R$0.10' => '10x15',
					'12x15: R$0.15' => '12x15',
					'13x18: R$0.20' => '13x18',
				]
			])
			->add('filtro', ChoiceType::Class, [
				'choices' => [
					'Grayscale' => 'gs',
					'Borrar' => 'br',
				]
			])
			->add('qtd', IntegerType::class, [
				'label' => 'Quantidade',
			])
            ->add('save', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Imagem::class,
        ]);
    }
}
