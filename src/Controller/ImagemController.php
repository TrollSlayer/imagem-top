<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;

use App\Entity\Imagem;
use App\Form\ImageFormType;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

use Imagine\Imagick\Imagine;

class ImagemController extends AbstractController
{
    /**
     * @Route("/imagem", name="imagem")
     */
    public function index()
    {
        return $this->render('imagem/index.html.twig', [
            'controller_name' => 'ImagemController',
        ]);

    }

 	/**
     * @Route("/", name="app_imagem_new")
     */
	public function new(Request $req) {
        $entityManager = $this->getDoctrine()->getManager();

		$imagem = new Imagem();

		$form = $this->createForm(ImageFormType::class, $imagem);
		$form->handleRequest($req);

		if ($form->isSubmitted() && $form->isValid()) {
			//** @var UploadedFile $file */
			
			$file = $form['img']->getData();
			$tm = $form['tamanho']->getData();
			$qtd = $form['qtd']->getData();
			$filtro = $form['filtro']->getData();
			$total = 0;
		
			$imagem->setData($file);
			$imagem->setTamanho($tm);

			if ($tm == '10x15') {
				$imagem->setPrecoTotal($qtd * 0.1);
			} else if ($tm == '12x15') {
				$imagem->setPrecoTotal($qtd * 0.15);
			} else if ($tm == '13x18') {
				$imagem->setPrecoTotal($qtd * 0.20);
			}

			$x = fopen('temp.jpg', 'wb');
			fwrite($x, $imagem->getData());
			fclose($x);

			/*
		 */
			try {

				$entityManager->persist($imagem);
				$entityManager = $this->getDoctrine()->getManager();
				$entityManager->flush();

			} catch (ORMException $e) {
				return new Response('Erro: '.$e);
			}

			// ================================================
			$repo = $this-> getDoctrine()->getRepository(Imagem::class);

			$res = $repo->find($imagem->getId());
			$file = $res->getData();
			$file->move($this->getParameter('temp_dir'), 'temp.jpg');

			$ed = new Imagine();
			$ed_imagem = $ed->open('temp/temp.jpg');

			if ($filtro == 'gs') {
				$fl = 'Grayscale';
				$ed_imagem->effects()->grayscale();
			} else if ($filtro == 'br') {
				$fl = 'Borrado';
				$ed_imagem->effects()->blur(3);
			}

			$ed_imagem->save('temp/temp_edited.jpg');
	
			return new Response('
			<html>
				<head></head>
				<body>
					<h2>Foto editada!!</h2>
					<table>
						<tr>
							<th>Quantidade</th>
							<th>Tamanho</th>
							<th>Filtro escolhido</th>
							<th>Preço</th>
							<th>Preview</th>
						</tr>
						<tr>
							<td>'.$imagem->getQtd().'</td>
							<td>'.$imagem->getTamanho().'</td>
							<td>'.$fl.'</td>
							<td>R$'.$imagem->getPrecoTotal().'</td>
							<td>
								<img src="'.$this->getParameter('temp_dir').'/temp_edited.jpg"/>
							</td>
						</tr>
					</table>
				</body>
			</html>
');
		}

		return $this->render('imagem/new.html.twig', [
			'form' => $form->createView(),
		]);
	}
	

	//Não finalizado!!!!!
	/**
     * @Route("/imagem/upload", name="app_imagem_upload")
     */
	public function upload_public() {
		$repo = $this-> getDoctrine()->getRepository(Imagem::class);

		$x_file = file_get_contents('temp/temp_edited.jpg');
		
		$url = 'https://api.imgbb.com/1/upload?key=c7b33906abd26b69eb076b92e135b661';

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => array('image' => base64_encode($x_file))
		));

		$res = curl_exec($curl);
		curl_close($curl);

		$pos = strpos($res, 'url":');
		$final_pos = strpos($res, ',"display_url');
		
		$u = substr($res, $pos+5, $final_pos);

		//$x = json_decode($res);

		return new Response((string)$u);

	}

}
