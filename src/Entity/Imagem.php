<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImagemRepository")
 */
class Imagem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="blob")
     */
    private $data;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nome;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $tamanho;

    /**
     * @ORM\Column(type="integer")
     */
    private $qtd;

    /**
     * @ORM\Column(type="float")
     */
    private $preco_total;

    /**
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    private $filtro;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getNome(): ?string
    {
        return $this->nome;
    }

    public function setNome(?string $nome): self
    {
        $this->nome = $nome;

        return $this;
    }

    public function getTamanho(): ?string
    {
        return $this->tamanho;
    }

    public function setTamanho(string $tamanho): self
    {
        $this->tamanho = $tamanho;

        return $this;
    }

    public function getQtd(): ?int
    {
        return $this->qtd;
    }

    public function setQtd(int $qtd): self
    {
        $this->qtd = $qtd;

        return $this;
    }

    public function getPrecoTotal(): ?float
    {
        return $this->preco_total;
    }

    public function setPrecoTotal(float $preco_total): self
    {
        $this->preco_total = $preco_total;

        return $this;
    }

    public function getFiltro(): ?string
    {
        return $this->filtro;
    }

    public function setFiltro(?string $filtro): self
    {
        $this->filtro = $filtro;

        return $this;
    }
}
